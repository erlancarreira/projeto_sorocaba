<?php require_once __DIR__.'/../Controllers/Cards/list.php'; ?>
 
<!doctype html>
<html lang="pt-br">
    <head>
	    <!-- Required meta tags -->
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	    <!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../../node_modules/bootstrap/dist/css/bootstrap.min.css">
		
		<!-- Style CSS -->
		<!-- <link rel="stylesheet" type="text/css" href="../assets/css/style.css"> -->

	    <title>Painel | Personagem listar</title>     
    </head>
    <body>

    <?php include_once(__DIR__.'/../layout/menu.php'); ?>  
   
	    <div class="container mt-5 mb-5"> 
		    
	        <h4 class="mb-3">Listar Personagem</h4>
	        
	        <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg']['text'])): ?>
	        
	        <div class="alert <?= $_SESSION['msg']['class']; ?> alert-dismissible fade show" role="alert">
			
			    <?= $_SESSION['msg']['text']; ?> 
			
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			    </button>
			
			</div>		
	        <?php endif ?>
	        
	        <?php if (count($rows)  > 0):  ?> 
            
               
	        <form method="POST">	
				<div class="row mb-5">
                
				<?php foreach ($rows['rows'] as $char): ?>
					
					<div class="col-md-4 mb-3">
						<div class="card">
						    
						    <div class="card-body">	
						    	<img src="../public/assets/images/<?= (!empty($char['image'])) ? $char['image'] : 'img.jpg'; ?>" class="img-fluid">	    
						    	<p><?= $char['text']; ?></p>    
			                    <a href="<?= URL; ?>/public/personagem_editar.php?id=<?= $char['id']; ?>" class="client btn-sm btn btn-primary">Editar</a>
			                    <button name="id" value="<?= $char['id']; ?>" class="client btn-sm btn btn-danger">Deletar</button>
						    </div>

						</div>
				    </div>
				
				<?php endforeach ?>
				
				</div> 
			</form>
			
			
			<?php if (isset($rows['total']) AND $rows['total'] > 3): ?>
	        
			<nav aria-label="Page navigation example">
			    <ul class="pagination justify-content-center">
			    
			    	<?php for ($i=1; $i <= $rows['paginate']; $i++): ?>
				    
				    <li class="page-item <?= ($i == $rows['currentPage']) ? 'active' : ''?>">
				    	<a class="page-link" href="<?= URL; ?>/public/personagem_listar.php?page=<?= $i; ?>"><?= $i; ?></a>
				    </li>

				    <?php endfor ?>

			    </ul>
			</nav>
			
			<?php endif ?>   

			<?php else: ?>

				<h5>Nenhum produto encontrado!</h5> 
			
			<?php endif ?>	

		</div>

	    <?php include_once(__DIR__.'/../layout/footer.php'); ?>
	    
	    <!-- Optional JavaScript -->
	    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	    <script src="../../node_modules/jquery/dist/jquery.slim.min.js" ></script>
	    <script src="../../node_modules/popper.js/dist/umd/popper.min.js" ></script>
	    <script src="../../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	    
    </body>
</html>


