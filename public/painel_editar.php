<?php require_once __DIR__.'/../Controllers/update.php'; ?>

<!doctype html>
<html lang="pt-br">
    <head>
	    <!-- Required meta tags -->
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	    <!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
		
		<!-- Style CSS -->
		<!-- <link rel="stylesheet" type="text/css" href="./assets/css/style.css"> -->

	    <title>Game | Cadastrar Card</title>     
    </head>
    <body>

	    <?php include_once __DIR__.'/../layout/menu.php'; ?>  
	    
	    <div class="container mt-5 mb-5"> 
		    <h4 class="mb-3">Atualizar</h4>
	        
	        <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg']['text'])): ?>
	        
	        <div class="alert <?= $_SESSION['msg']['class']; ?> alert-dismissible fade show" role="alert">
			  
			    <?= $_SESSION['msg']['text']; ?> 
			  
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			    </button>
			</div>
	        
	        <?php endif ?>
            
            
            <?php if (count($rows)): ?>
			
			<form method="POST" enctype="multipart/form-data" >  
			    <div class="row mb-3" >
			    	<div class="col-md-9">
				    	<div class="card ">
				        
				        	<img src="./assets/images/<?= (!empty($rows['background_url'])) ? $rows['background_url'] : 'img.jpg' ; ?>" class="card-img-top img-fluid " id="imgPrev_background" alt="..." >
				        	<div class="card-img-overlay">
							    <div class="carousel-caption d-none d-md-block">
								    <h3 class="text-white">Imagem de fundo</h3>
								    
								</div>
							</div>
						</div>	
				    </div>

					<div class="col-md-3">
			        	<div class="card">
				        	<img src="./assets/images/<?= (!empty($rows['main_image'])) ? $rows['main_image'] : 'img.jpg' ; ?>" height="400" class="card-img-top img-fluid imgPrev" id="imgPrev_main" alt="...">
				        	<div class="card-img-overlay">
							    <div class="carousel-caption d-none d-md-block">
								    <h3 class="text-white">Imagem de destaque</h3>
								    
								</div>
							</div>
						</div>
					</div>
			    </div>
			    <div class="row mb-3">
	        		   
					<div class="col-md-12" > 
					    <div class="form-group">
			                <label >Nome do jogo</label>
			                <input id="jogo_nome" type="text" class="form-control" name="name" placeholder="Nome" value='<?= $rows['name']; ?>' />

					    </div>	
                       
					    <div class="form-group">
					    	<label >Frase</label>
			                <input id="frase" type="text" class="form-control" name="description" value='<?= $rows['description']; ?>' placeholder="Descrição" />
					    </div>	

					    <div class="form-group">
					    	<label >Descrição do formulário</label>
			                <input id="form_descricao" type="text" class="form-control" name="description_form" value='<?= $rows['description_form']; ?>' placeholder="Descrição do Formulário" />
					    </div>	
				    </div>

				    <div class="col-md-6" > 
					    
					    <div class="form-group">
					    	<label>Imagem de fundo do Painel</label>
						    <input type="file" class="form-control-file" id="background_url" name="background_url" >
					    </div>	

				    </div>

				    <div class="col-md-6" > 
					    
					    <div class="form-group">
					    	<label>Imagem de destaque</label>
						    <input type="file" class="form-control-file" id="main_image" name="main_image" >
					    </div>	

				    </div>
			    </div>
           
			    <button type="submit" class="btn btn-success btn-block">Atualizar</button>

			</form>
	      	<?php endif; ?>
		</div>

	    <?php include_once __DIR__.'/../layout/footer.php'; ?>
	    <!-- Optional JavaScript -->
	    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	    <script src="../node_modules/jquery/dist/jquery.slim.min.js" ></script>
	    <script src="../node_modules/popper.js/dist/umd/popper.min.js" ></script>
	    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	    <script src="./assets/js/preview.js"></script>
	    <script>
	    
	    	$(document).ready( function() {
	            
	            var $imgPrev = ''

	            $(document).on('change', '#main_image, #background_url', function (event) {
	                     
	                $imgPrev = (event.target.id == 'main_image') ? '#imgPrev_main' : '#imgPrev_background'
	                $('.row ' + $imgPrev).show()
	                ReadURL(event.target, $imgPrev)
	            }) 
	    	})
	    </script>
	    
	    
	   
    </body>
</html>


