function ReadURL(input, id) {
	
	var inputFiles = input.files

    if (inputFiles && inputFiles[0]) {
        
        inputExt = inputFiles[0].type.split('/')
        
        var reader = new FileReader()
        
        var ext = ['jpg', 'jpeg', 'png']
        
        if (!ext.includes(inputExt[1])) 
            return
               
        reader.onload = function (e) {
           
            $(id).attr('src', e.target.result)
           
        }

        reader.readAsDataURL(input.files[0])
    }
}

