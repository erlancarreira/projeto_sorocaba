<?php 

include_once __DIR__.'../../config.php';
include_once __DIR__.'/../Model/Char.php';


if (isset($_POST) && !empty($_POST)) {
    
    $db = new DB();
       
    $request = [
        'char_text'  => $_POST['texto'],
        'char_image' => $_FILES['imagem']
    ];

    
    
    if (!Validator::checkRequest($request)) {
       
        $file = new File($request['char_image']);
        $file->upload();
        
        $request['char_image'] = $file->getNewImage();

        if (save ( new Char($request) )) {
        	Validator::setMsg("Personagem cadastrado com sucesso!", 'alert-success');
        }
       
    }

    	
}

?>