<?php 

    include_once __DIR__.'../../config.php';
    include_once __DIR__.'/../Model/Panel.php';

    $id = (isset($_GET['id']) && !empty($_GET['id'])) ? (int) addslashes($_GET['id']) : '';
    
    $rows = show(1);

    if (isset($_POST) && !empty($_POST)) {
        
    	$request = [
	        'id'               => $rows['id'],
	        'name'             => $_POST['name'],
            'description'      => $_POST['description'],
            'description_form' => $_POST['description_form'],
		    'background_url'   => $_FILES['background_url'] ?? "",	
            'main_image'       => $_FILES['main_image'] ?? "",       
	    ]; 
      
	    
	    if (!empty($request['background_url']['tmp_name'])) {
            
            $file = new File($request['background_url']);
            
            if (empty($file->Allowed())) {

                Message::setMsg("Extensão não permitida!", 'alert-danger');
                 
            } else {

                
                $file->upload();
                
                unlinkImage(getImage('background_url', $rows['background_url']));                
                
                $request['background_url'] = $file->getNewImage();

            }
    	
        } else {

            $request['background_url'] = $rows['background_url'];
        }

        if (!empty($request['main_image']['tmp_name'])) {
            
            $file = new File($request['main_image']);

            if (empty($file->Allowed())) {

                Message::setMsg("Extensão não permitida!", 'alert-danger');
            
            } else {    
            
                $file = new File($request['main_image']);
                
                $file->upload();
                unlinkImage(getImage('main_image', $rows['main_image']));

                $request['main_image'] = $file->getNewImage();
            }
        
        } else {
            
            $request['main_image'] = $rows['main_image'];
        }
               
         
        if (save($request)) {     
    		
            header('Location: painel_editar.php'); 
    	}
        
       
    }

    
?>