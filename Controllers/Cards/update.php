<?php 

    include_once __DIR__.'./../../config.php';
    include_once __DIR__.'/../../Model/Char.php';

    $id = (isset($_GET['id']) && !empty($_GET['id'])) ? (int) addslashes($_GET['id']) : '';
    
    $rows = show($id);
    
    if (isset($_POST) && !empty($_POST)) {
        
    	$request = [
	        'id'    => $rows->getId(),
	        'text'  => $_POST['text'],
		    'image' => $_FILES['image'],	
            'table' => $rows->getTable()	    
	    ]; 
      
            
        if (!empty($request['image']['tmp_name'])) {
            $file = new File($request['image']);
            $file->upload();
            $request['image'] = $file->getNewImage();
    	} else {
            $request['image'] = $rows->getImage();
        }

        if (save(new Char($request))) {     
    		
            header('Location: personagem_listar.php'); 
    	}
        //} 
       
    }

    
?>