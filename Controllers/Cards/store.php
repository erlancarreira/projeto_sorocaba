<?php 

include_once __DIR__.'../../../config.php';
include_once __DIR__.'/../../Model/Char.php';


if (isset($_POST) && !empty($_POST)) {
    
    $db = new DB();
       
    $request = [
        'text'  => $_POST['text'],
        'image' => $_FILES['image']
    ];

    if (isset($request['text'], $request['image']) && !empty($request['text']) && !empty($request['image']['tmp_name'])) {
        
        $file = new File($request['image']);
        
        $file->upload();

        if (empty($file->Allowed())) {

            Message::setMsg("Extensão não permitida!", 'alert-danger');
       
        } else {  
            $file->upload();
            $request['image'] = $file->getNewImage();
            if (save ( new Char($request)) ) {
                Message::setMsg("Personagem cadastrado com sucesso!");
            } 
        }   
         

    } else {
        Message::setMsg("Os campos são requeridos!", 'alert-danger');    
    }

    	
}

?>