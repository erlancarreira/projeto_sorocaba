# Simples Crud em Php 

- Instruções
   - Importe o banco de dados em Dump/game_db.sql.
   - Especifique o host, username e password do seu servidor em env.php.
   - Rode o comando npm install para adicionar os arquivos requeridos pelo bootstrap.

# Estrutura

- Pastas
	- Class  
        - Db.class.php 
        - Char.class.php
        - Connection.class.php
        - DB.class.php 
	- Layout 
        - Footer.php 
        - Menu.php 
	- Libs 
        - Message.class.php 
        - Files.class.php
    - Model  
        - Char 
            - Save 
            - Delete 
            - Show  
            - Paginate
        - Panel    
    - Controllers
        - Cards
            - list
            - store
            - updat   
    - Public 
        - Views
            - cadastrar
            - listar
            - editar
        - Assets 
            - Images 
            - Css 
            - Js     
    - Layouts
        - menu.php
        - footer.php 
    - Dump
        - game_db.sql 

       
- Arquivos
    - env.php
    - config.php                          
    - package.json 


