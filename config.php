<?php

if (!defined('URL')) { define('URL', "http://".$_SERVER['HTTP_HOST']); }

spl_autoload_register(function($value) {

    $Libs  = __DIR__.DIRECTORY_SEPARATOR."Libs".DIRECTORY_SEPARATOR.$value.".class.php";
    
    if (file_exists($Libs)) {
    	include_once $Libs;     
    }    
});

spl_autoload_register(function($value) {
    
    $Class = __DIR__.DIRECTORY_SEPARATOR."Class".DIRECTORY_SEPARATOR.$value.".class.php";
    $Libs  = __DIR__.DIRECTORY_SEPARATOR."Libs".DIRECTORY_SEPARATOR."File.class.php";
    $Env   = __DIR__.DIRECTORY_SEPARATOR.'.'.DIRECTORY_SEPARATOR.'.'.DIRECTORY_SEPARATOR.'env.php';

    if (file_exists($Env) && file_exists($Class)) {

    	include_once $Env;
    	include_once $Libs;
        require_once $Class;   
    }
    
});




