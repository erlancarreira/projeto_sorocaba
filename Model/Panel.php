<?php 

include_once __DIR__.'../../config.php';

function save($request) {
    
    $db = new DB();
     
    if ($request['id']) {
        
        $db->query("
            UPDATE 
                configs
            
            SET 
                name = :name, 
                description = :description, 
                description_form = :description_form, 
                main_image = :main_image, 
                background_url = :background_url 

            WHERE 
                id = :id", 
                [   
                    ':id'               =>  $request['id'],
                    ':name'             =>  $request['name'],
                    ':description'      =>  $request['description'],
                    ':description_form' =>  $request['description_form'],
                    ':main_image'       =>  $request['main_image'],
                    ':background_url'   =>  $request['background_url']
                ]
            );
        
    } 
   
    return !! $db->getCount();      
}

function show($id) {
    
    $db = new DB();

    $rows = $db->select("SELECT * FROM configs WHERE id = :id", [':id' => $id]);
     
    return ($db->getCount() > 0) ? $rows[0] : [];  
}


// function Paginate($table, $page, $search = '', $itemsPerPage = 3) {  
        
//     $db = new DB();

//     $start = 0;
     
//     if (isset($page) && !empty($page)) {
    
//         $start = ($page - 1) * $itemsPerPage;
//     } 

//     $where = (!empty($search)) ? " WHERE name LIKE :search" : '';

//     $rows = (array) $db->select("SELECT sql_calc_found_rows * FROM $table $where ORDER BY id LIMIT $start, $itemsPerPage", [
//             ':search' => '%'.$search.'%' ]);

//     $totalRows = $db->select('SELECT found_rows() as totalCount')[0];

//     return [
//         'rows'        => $rows,
//         'paginate'    => ceil($totalRows['totalCount'] / $itemsPerPage),
//         'total'       => $totalRows['totalCount'],
//         'currentPage' => $page 
//     ];   
// }

function getImage($name, $ref) {
    
    $db = new DB();

    $image = $db->select("SELECT $name FROM configs WHERE background_url = :image OR main_image = :image", [':image' => $ref ]);

    return ($db->getCount()) ? $image[0] : []; 
    
}

function unlinkImage($ref = null) {

    $path = './assets/images/';
    
    foreach ($ref as $value) {
        
        if (!empty($value)) {
            
            array_map("unlink", glob($path.$value));
        }
    } 

}

?>