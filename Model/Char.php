<?php 

include_once __DIR__.'../../config.php';

function save(Char $char) {
    
    $db = new DB();
    
    try {
             
        if ($char->getId()) {
            
            $db->query("UPDATE {$char->table} SET text = :text, image = :image WHERE id = :id", [
                    ':id'     =>  $char->getId(),
                    ':text'   =>  $char->getText(),
                    ':image'  =>  $char->getImage()
                ]
            );
            
        } else {
            
            $db->query("INSERT INTO {$char->table} (text, image) VALUES (:text, :image)", 
                [
                    ':text'  =>  $char->getText(),
                    ':image' =>  $char->getImage()
                ]
            );  
        }
       
        return !! $db->getCount();   
    
    } catch (Exception $e) {
        
        echo json_encode($e);     
    }     
}

function show($id) {
    
    $db = new DB();

    $rows = $db->select("SELECT * FROM cards WHERE id = :id", [':id' => $id]);
     
    return ($db->getCount() > 0) ? new Char($rows[0]) : $rows;  
}

function delete($id) {
    
    $db = new DB();

    unlinkImage(getImage($id));
    
    $db->query("DELETE FROM cards WHERE id = :id", [':id' => $id ]); 
    
     
    return !! $db->getCount();  
}

function Paginate($table, $page, $search = '', $itemsPerPage = 3) {  
        
    $db = new DB();

    $start = 0;
     
    if (isset($page) && !empty($page)) {
    
        $start = ($page - 1) * $itemsPerPage;
    } 

    $where = (!empty($search)) ? " WHERE name LIKE :search" : '';

    $rows = (array) $db->select("SELECT sql_calc_found_rows * FROM $table $where ORDER BY id LIMIT $start, $itemsPerPage", [
            ':search' => '%'.$search.'%' ]);

    $totalRows = $db->select('SELECT found_rows() as totalCount')[0];

    return [
        'rows'        => $rows,
        'paginate'    => ceil($totalRows['totalCount'] / $itemsPerPage),
        'total'       => (int) $totalRows['totalCount'],
        'currentPage' => (int) $page 
    ];   
}

function getImage($id = null) {
    
    $db = new DB();

    $image = $db->select("SELECT image FROM cards WHERE id = :id", [':id' => $id ]);

    return ($db->getCount()) ? $image[0] : []; 
}

function unlinkImage($ref = null) {
    
    $path = './assets/images/';
    
    foreach ($ref as $value) {
        
        if (!empty($value)) {
            
            array_map("unlink", glob($path.$value));
        }
    } 
  
}

?>