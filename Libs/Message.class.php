<?php 

/**
 * 
 */
class Message 
{
	 
	const MSG = 'msg';

 	public static function setMsg($msg, $class = 'alert-success') 
    {
    	$_SESSION[Message::MSG] = ['text' => $msg, 'class' => $class];  
    }

    public static function getMsg() 
    {
        $msg = (isset($_SESSION[Message::MSG]) && !empty($_SESSION[Message::MSG])) ? $_SESSION[Message::MSG] : "";
        
        Message::clearMsg();
        
        return $msg;
    }

    public static function clearMsg() 
    {
        $_SESSION[Message::MSG] = NULL;
    }

    public static function setError($message, $alert) {

        Message::setMsg($message, $alert); 
    }
}

?>