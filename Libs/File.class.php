<?php 
class File {
    
    private $tmpName;
    private $destinationPath;
    private $name; 
    private $size;
    private $type;
    private $error;
    private $isMimeValid;
    private $imageSize;
    private $mime;
    private $allowed;
    private $newImage;
    private $messageError; 
    //TODO: GETS and SETS

    public function __construct(array $file) 
    {
    	$this->allowed  = ['jpeg', 'jpg', 'png', 'gif'];
        $this->setName($file['name']);
        $this->setType($file['type']);
        $this->setTmpName($file['tmp_name']);
        $this->setError($file['size']);
        $this->setSize($file['size']);
        $this->setImageSize($this->tmpName);
        $this->setDestinationPath('./assets/images'); 
        $this->setMime(); 
        
        	
    }

    public function setTmpName($tmpName) {
        $this->tmpName = $tmpName;
    }

    public function getTmpName($tmpName) {
        return $this->tmpName;
    }

    public function setName($name) {
        
        $name = explode('.', $name);  

        if (isset($name[0])) { 
            $this->name = $name[0]; 
        } 
    }

    public function getName($name) {
        return $this->name;
    }

    public function setSize($size) {
        $this->size = $size;
    }

    public function getSize($size) {
        return $this->size;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getType($type) {
        return $this->type;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function getError($error) {
        return $this->error;
    }

    

    public function getDestinationPath()
    {
        return $this->destinationPath;
    }

    public function setDestinationPath($destinationPath)
    {
        $this->destinationPath = $destinationPath;
    }

    public function setImageSize($file) 
    {   
        $this->imageSize = (!isset($file) || empty($file)) ? null : getimagesize($file);
    }

    public function getImageSize() 
    {
        return $this->imageSize;
    }

    public function setMime() 
    {
        $mime = explode('/', $this->imageSize['mime']);
            
        $this->mime = (isset($mime[1])) ? $mime[1] : null;
        
    }

    public function getMime() 
    {
        return $this->mime;
    }

    public function setNewImage($image) 
    {   
       
        if (!empty($image)) { 
            $this->newImage = $image . '_' . md5(time()).'.jpg'; 
        }
    }

    public function getNewImage() 
    {
        return $this->newImage;
    }    

    public function Allowed() 
    {   
        if ($this->mime != null) {
            if (in_array($this->mime, $this->allowed)) {

                return true;
            }
        }
    }

    public function moveUploadedFile() 
    {
        
        $this->setNewImage($this->name);

        if (!empty($this->getNewImage())) {
        
            $destiny = $this->getDestinationPath() . DIRECTORY_SEPARATOR . $this->getNewImage(); 

            if (move_uploaded_file($this->tmpName, $destiny)) {
            	return true;
            }
        }      
        
    }

    public function upload()
    {   
       return $this->moveUploadedFile();  
    } 

   


  
}
?>