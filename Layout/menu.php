<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="http://<?= $_SERVER['HTTP_HOST']; ?>/public/personagem_listar.php">Painel PHP</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">        
        
        <li class="nav-item ">
          <a class="nav-link " href="http://<?= $_SERVER['HTTP_HOST']; ?>/public/painel_editar.php">Configurações</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Personagem</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="http://<?= $_SERVER['HTTP_HOST']; ?>/public/personagem_cadastrar.php">Cadastrar</a>
            <a class="dropdown-item" href="http://<?= $_SERVER['HTTP_HOST']; ?>/public/personagem_listar.php">Listar</a>
          </div>
        </li>
      </ul>
      
    </div>
  </div>
</nav>
