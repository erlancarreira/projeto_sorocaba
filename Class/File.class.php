<?php 






class File {
    
    private $tmpName;
    private $destinationPath;
    private $name; 
    private $size;
    private $type;
    private $error;
    private $isMimeValid;
    private $imageSize;
    private $mime;
    private $allowed;
    private $newImage;
    private $messageError;
    //TODO: GETS and SETS

    public function __construct(array $file) 
    {
    	$this->allowed  = ['jpeg', 'jpg', 'png', 'gif'];

        $this->setTmpName($file['tmp_name']);
    	$this->setName($file['name']);
        $this->setSize($file['size']);
        $this->setType($file['type']);
        $this->setError($file['size']);  
        $this->setImageSize($this->tmpName); 
        $this->setMime(); 	
    }

    public function setTmpName($tmpName) {
        $this->tmpName = $tmpName;
    }

    public function getTmpName($tmpName) {
        return $this->tmpName;
    }

    public function setName($name) {
        
        $name = explode('.', $name);  

        if (isset($name[0])) { 
            $this->name = $name[0]; 
        } 
    }

    public function getName($name) {
        return $this->name;
    }

    public function setSize($size) {
        $this->size = $size;
    }

    public function getSize($size) {
        return $this->size;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getType($type) {
        return $this->type;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function getError($error) {
        return $this->error;
    }

    

    public function getDestinationPath()
    {
        return $this->destinationPath;
    }

    public function setDestinationPath($destinationPath)
    {
        $this->destinationPath = $destinationPath;
    }

    public function setImageSize($file) 
    {   
        $this->imageSize = (!isset($file) || empty($file)) ? null : getimagesize($file);
    }

    public function getImageSize() 
    {
        return $this->imageSize;
    }

    public function setMime() 
    {
        $mime = explode('/', $this->imageSize['mime']);
            
        if (isset($mime[1])) { 
            $this->mime = $mime[1]; 
        } 
        
    }

    public function getMime() 
    {
        return $this->mime;
    }

    public function setNewImage($image) 
    {   
       $this->newImage = $image . '_' . md5(time()).'.jpg'; 

    }

    public function getNewImage() 
    {
        return $this->newImage;
    }    

    public function Allowed() 
    {   
        //var_dump($this->mime);
        if (in_array($this->mime, $this->allowed)) {

            return true;
        }
    }


    public function dirExists() 
    {
    	if (!isset($this->destinationPath)) { 
            $this->setMessageError = "Destination path without value. Did you put a target folder with the setDestinationPath() method?"; 
        }
    	return true; 
    }

    public function createDir() 
    {
    	if (!isset($this->destinationPath)) {

            $this->setMessageError = "Destination path without value. Did you put a target folder with the setDestinationPath() method?";
    	}

    	if (!$this->dirExists()) {
             
            if (!mkdir($this->destinationPath, 0777, true)) {
                $this->setMessageError = "Error during create a directory")); 
            }
    	}

    	return true;
    }

    public function moveUploadedFile() 
    {
        
        //$this->createDir();
        $this->setNewImage($this->name);

        $destiny = $this->destinationPath . DIRECTORY_SEPARATOR . $this->getNewImage(); 

        if (!move_uploaded_file($this->tmpName, $destiny)) {
        	$this->setMessageError = "The filename is not a valid upload file or cannot be moved for some reason.";
        }

        return $this->getMessageError();
    }

    public function upload()
    {   
        try {
            $this->moveUploadedFile();
        } catch (ErrorException $e) {
            return $this->setMessageError($e);
        }  
    } 

    public function setMessageError($e) {
        var_dump($e);
        $this->messageError = json_encode([
            "message" => $e->getMessage(),
            "line"    => $e->getLine(),
            "file"    => $e->getFile(),
            "code"    => $e->getCode() 
        ]);
    }

    public function getMessageError() {
        return $this->messageError;
    }


  
}
?>