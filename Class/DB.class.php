<?php 
/**
 * 
 */

class DB extends Connection
{
	

    function __construct() { 
        
        DB::getConn();

    }
    
    private function setParams($statement, $paramets = array())  
	{
       
        foreach ($paramets as $key => $value) {
          	
          	$this->bindParam($statement, $key, $value);
        
        }
	}

    private function bindParam($statement, $key, $value) 
    {  
        
        $statement->bindParam($key, $value); 
    }

    public function query($rowQuery, $paramets = array()) 
    {
    	
        $stmt = self::getConn()->prepare($rowQuery);
     
    	$this->setParams($stmt, $paramets);

    	$stmt->execute();

        return $this->setCount($stmt->rowCount()); 
    }

    public function select($rowQuery, $params = array()) 
    {
        $stmt = self::getConn()->prepare($rowQuery); 

        $this->setParams($stmt, $params);

        $stmt->execute();
        
        $this->setCount($stmt->rowCount()); 

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    private function setCount($count) 
    {
        $this->count = $count;
    }

    public function getCount() 
    {
        return $this->count;
    } 

    
}


?>