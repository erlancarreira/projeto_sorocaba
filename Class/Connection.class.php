<?php 

/**
 * 
 */
abstract class Connection 
{
	private static $conn;

	protected static $errors;
   
	protected static function getConn() {
		
		if (empty(self::$conn)) {   

		    try {
	    		self::$conn = new PDO( 
                    getenv('DB_DRIVER')
                    .":host=".
                    getenv('DB_HOST')
                    .":".
                    getenv('DB_PORT')
                    .";dbname=".
                    getenv('DB_NAME'), 
                    getenv('DB_USERNAME'), 
                    getenv('DB_PASSWORD'),
                    [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]
                );
                
                self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 

	    	} catch (Exception $e) {
	    	   
	    	    print "Ocorreu um erro: ".$e->getMessage(); 		    
            }		    
	    }

	    return self::$conn; 	
	}

	public function getError() 
    {
        return $this->errors;
    }  
    

}

?>