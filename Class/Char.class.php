<?php 
/**
 * 
 */
class Char 
{   
	private $id;
	private $text;
	private $image;
	public  $table = 'cards';

	function __construct(array $request)
	{		
		if (isset($request['id']) && !empty($request['id'])) { 
			$this->id = $request['id']; 
		}

		$this->text  = $request['text'];
		$this->image = $request['image'];
	}

	public function setId($id) 
	{
        $this->id = $id;
	}
    
    public function getId() 
    {
		return $this->id;
	}

	public function getText() 
	{
		return $this->text;
	}

	public function getImage() 
	{
		return $this->image;
	}

	public function getTable() {
		return $this->table;
	}

	
}
?>